//Open a modal and generate the multicriteria values
$("#modal_opener").click(function () {
    var url = "/api/multi"; // pass data like this ?data='+text
    // AJAX START

    $.get(url, function (data) {
        let testr = "";
        $.each(data, function (i) {
            testr = testr.concat("<tr class='test'> <th>");
            testr = testr.concat(data[i].id);
            testr = testr.concat("</th>\n");
            testr = testr.concat("<th> <input type='text' class='form-control' value='");
            testr = testr.concat(data[i].ccMin);
            testr = testr.concat("'>");
            testr = testr.concat("</th></input>\n");
            testr = testr.concat("<th> <input type='text' class='form-control' value='");
            testr = testr.concat(data[i].ccMax);
            testr = testr.concat("'>");
            testr = testr.concat("</th>\n");
            testr = testr.concat("<th> <input type='text' class='form-control' value='");
            testr = testr.concat(data[i].carMinAge);
            testr = testr.concat("'>");
            testr = testr.concat("</th>\n");
            testr = testr.concat("<th> <input type='text' class='form-control' value='");
            testr = testr.concat(data[i].carMaxAge);
            testr = testr.concat("'>");
            testr = testr.concat("</th>\n");
            testr = testr.concat("<th> <input type='text' class='form-control' value='");
            testr = testr.concat(data[i].baseAmount);
            testr = testr.concat("'>");
            testr = testr.concat("</th>\n");
            testr = testr.concat("</tr>\n");
        })

        $("#tbadi").append(testr)
    })
});

//clear table when closing the modal
$(".modal").on("hidden.bs.modal", function () {
    $("#tbadi").html("");
    // $("th").val("");
});

$("#save_table").click(function () {
    alert('im alerted wow')
    var url = "/api/multi";


    const getAllAttributes = el => el
        .getAttributeNames()
        .reduce((obj, name) => ({
            ...obj,
            [name]: el.getAttribute(name)
        }), {})

    let multicriteriasToUpdate = [];

    $(".test").each(function (e) {
        multicriteriasToUpdate[e] = {
            "id": $(this).find("th:eq(0)").text(),
            "ccMin": $(this).find("th:eq(1) input[type=text]").val(),
            "ccMax": $(this).find("th:eq(2) input[type=text]").val(),
            "carMinAge": $(this).find("th:eq(3) input[type=text]").val(),
            "carMaxAge": $(this).find("th:eq(4) input[type=text]").val(),
            "baseAmount": $(this).find("th:eq(5) input[type=text]").val()
        }

        // Object.entries(data).forEach(([key, value]) => {
        //     if (data !== undefined){
        //      console.log(`${key} ${value}`);
        //     }})
    })

    $.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify(multicriteriasToUpdate),
        error: function (e) {
            console.log(e);
        },
        dataType: "json",
        contentType: "application/json"
    });
})