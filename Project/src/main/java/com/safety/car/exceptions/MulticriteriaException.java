package com.safety.car.exceptions;

public class MulticriteriaException extends RuntimeException{
    public MulticriteriaException(String message) {
        super(message);
    }
}
