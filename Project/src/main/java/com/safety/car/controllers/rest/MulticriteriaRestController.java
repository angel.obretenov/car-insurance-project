package com.safety.car.controllers.rest;

import com.safety.car.exceptions.EmptyException;
import com.safety.car.exceptions.MulticriteriaException;
import com.safety.car.models.entity.MulticriteriaTable;
import com.safety.car.services.interfaces.MulticriteriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/multi")
public class MulticriteriaRestController {
    private final MulticriteriaService multicriteriaService;

    @Autowired
    public MulticriteriaRestController(MulticriteriaService multicriteriaService) {
        this.multicriteriaService = multicriteriaService;
    }

    @GetMapping
    public List<MulticriteriaTable> getAll(){
       return multicriteriaService.getAll();
    }

    @GetMapping("/byCc")
    public Double getByCcAndAge(@RequestParam int cc, @RequestParam int age) {
        try {
            return multicriteriaService.getByCCAndAge(cc, age);
        } catch (EmptyException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public List<MulticriteriaTable> updateList(@RequestBody List<MulticriteriaTable> multiList){
        try{
            return multicriteriaService.update(multiList);
        } catch (MulticriteriaException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}