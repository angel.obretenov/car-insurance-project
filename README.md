# Car insurance web application

# **Project Description**
Web application through which you can insure your car and drive safely on the road.\
Basic users can simulate offer and request the offer if they like the price, if they somehow find a better deal\
they can of course cancel their policy by going in My policies tab and cancel it.\
Agents can see all of the requested policies and either reject or accept them, as of doing that the users get notified by email.

* Technologies used:
    - [x] Java 11
    - [x] IDE: IntelliJ IDEA
    - [x] Database: SQL & MariaDB
    - [x] Spring 
    - [x] Hibernate
    - [x] Thymeleaf template engine
    - [x] HTML and CSS with Bootstrap
    - [x] Swagger 

## Trello board
# [Click me](https://trello.com/b/eKg50OXy/car-insurance-project)
    
## Homepage
### In this page the unauthorised user can simulate its offer and see the price. If the price is okay they can click on Request policy or Service in the navbar and they will get redirected to login/register page.
![Index](/images/indexin.png)

## Policy request
### Authorised users will be redicted to this page so they can fill the rest of the data and request their policy. In the page as shown they can see their car details as well.
![Index](/images/requesting a policy.png)

## My policies
### As we said above if you find a better deal, you can go in My policies tab and cancel your policy or just make sure all of your details are written correctly.
![Index](/images/index.png)

## Policy request
### The tab policy request and tickets are only shown to the agents, they can go in Policy request to see all the pending requests and either reject or accept them. If they open Tickets they will see all of the tickets in the database and sort them by id or status (Like accepted/rejected/pending).
![Index](/images/admin approve reject.png)